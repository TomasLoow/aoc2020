import functools
from parsy import generate, regex

# File parsers definitions
from common_parsers import nl


@generate
def p_line():
    """Parser for a line of a-z values, returns a set of one-char strings"""
    chars = yield regex("[a-z]").times(1,30) << nl
    return set(chars)


@generate
def p_block_1():
    """Parser for a block of p_lines, combines the found pairs into a larger dictionary"""

    lines = yield p_line.many()
    return functools.reduce(lambda s1, s2: s1.union(s2), lines)

p_file_1 = p_block_1.sep_by(nl)  # Parser for the whole file for part 1


@generate
def p_block_2():
    """Parser for a block of p_lines, combines the found pairs into a larger set"""
    lines = yield p_line.many()
    return functools.reduce(lambda s1, s2: s1.intersection(s2), lines)

p_file_2 = p_block_2.sep_by(nl)  # Parser for the whole file for part 2

# END parser defintions


def run():
    input = open("input/day6.txt", "r").read()
    print("=== day 6 ===")

    groups_1 = p_file_1.parse(input)
    res = sum([len(s) for s in groups_1])
    print(f"Solution part 1: {res}")

    groups_2 = p_file_2.parse(input)
    res = sum([len(s) for s in groups_2])
    print(f"Solution part 2: {res}")


run()