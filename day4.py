import re

from parsy import generate, string

# File parsers definitions
from common_parsers import colon, non_space, nl, space

identifier = (
    string("byr")
    | string("iyr")
    | string("eyr")
    | string("hgt")
    | string("hcl")
    | string("ecl")
    | string("pid")
    | string("cid")
).desc("A valid identifier")


@generate
def value():
    """Parser for a k:v pair"""
    i = yield identifier
    yield colon
    v = yield non_space
    return (i, v)


@generate
def p_line():
    """Parser for a line of one or more k:v pairs separated by spaces, ending with a newline"""

    first = yield (value).desc("First value on line")
    rest = yield (nl | space >> (value.sep_by(space) << nl)).desc(
        "More values separated by spaces"
    )
    if rest == "\n":
        return [first]
    return [first] + rest


@generate
def p_block():
    """Parser for a block of p_lines, combines the found pairs into a dictionary"""

    lines = yield p_line.many()
    d = {}
    for line in lines:
        d = {**d, **dict(line)}
    return d


p_file = p_block.sep_by(nl)  # Parser for the whole file

# END parser defintions


def string_length_validator(length):
    def string_length(input):
        return len(input) == length

    return string_length


def int_range_validator(min, max):
    def int_range(input):
        return min <= int(input) <= max

    return int_range


def regex_validator(reg):
    def regex(input):
        match = re.match(reg, input)
        if not match:
            return False
        return True

    return regex


def oneof_validator(elems):
    def oneof(input):
        return input in elems

    return oneof


def height_validator():
    def height(input):
        match = re.match("^([\d]{1,3})([a-z]{2})$", input)
        if not match:
            return False
        l, u = match.groups()
        if u not in ["cm", "in"]:
            return False
        if u == "cm" and not (150 <= int(l) <= 193):
            return False
        if u == "in" and not (59 <= int(l) <= 76):
            return False
        return True

    return height


def validate_passport(passport):
    validators = {
        "byr": [int_range_validator(1920, 2002), string_length_validator(4)],
        "iyr": [int_range_validator(2010, 2020), string_length_validator(4)],
        "eyr": [int_range_validator(2020, 2030), string_length_validator(4)],
        "hgt": [height_validator()],
        "hcl": [regex_validator("^#[a-f0-9]{6}$")],
        "ecl": [oneof_validator(["amb", "blu", "brn", "gry", "grn", "hzl", "oth"])],
        "pid": [int_range_validator(0, 999999999), string_length_validator(9)],
    }
    for k, field_validators in validators.items():
        value = passport[k]
        for field_validator in field_validators:
            if not field_validator(value):
                raise ValueError(
                    f"field {k}:{value} failed for validator {field_validator.__name__}"
                )


def run():
    input = open("input/day4.txt", "r").read()
    passports = p_file.parse(input)

    needed_keys = set(("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"))

    print("=== day 4 ===")
    valid = []
    for p in passports:
        if needed_keys.issubset(set(p.keys())):
            valid.append(p)
    print("Solution part 1:{}".format(len(valid)))

    # Part 2
    passports = valid
    valid_pass2 = []
    for p in passports:
        try:
            validate_passport(p)
            valid_pass2.append(p)
        except ValueError as e:
            pass
    print("Solution part 2:{}".format(len(valid_pass2)))


run()
