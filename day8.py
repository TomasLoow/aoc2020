from typing import List, Tuple, Generator, Union, Set

from parsy import regex, string, seq, alt

from common_parsers import space, nl

# Type aliases

OpCode = str
Instruction = Tuple[OpCode, int]
Program = List[Instruction]


# File parsers definitions

p_signed_number = seq((string("+") | string("-")), regex("[0-9]+").map(int)).combine(lambda sign, num: num if sign=="+" else -num)
p_op = alt(string("jmp"), string("nop"), string("acc"))

p_instr = seq(p_op, space, p_signed_number).combine(lambda a, _b, c: (a,c))
p_file = p_instr.sep_by(nl) << nl # Parser for the whole file

# END parser defintions

def execute_part1(prog: Program) -> int:
    acc = 0
    idx = 0
    visited_rows: Set[int] = set()
    while True:
        if idx in visited_rows:
            return acc
        visited_rows.add(idx)
        op, val = prog[idx]
        if op == "nop":
            idx += 1
        elif op == "acc":
            acc += val
            idx += 1
        elif op == "jmp":
            idx += val
        else:
            raise ValueError("bad op code")


def mutations(prog: Program) -> Generator[Program, None, None]:
    """ generator for all single instrcution mutations of prog"""
    for (i, p_instr) in enumerate(prog):
        if p_instr[0] == "nop":
            new_instr = "jmp"
        elif p_instr[0] == "jmp":
            new_instr = "nop"
        else:
            continue
        yield prog[:i] + [(new_instr, p_instr[1])] + prog[i+1:]
    return



def execute_part2(prog: Program) -> Tuple[bool, Union[int, str]]:
    prog_size = len(prog)
    acc = 0
    idx = 0
    visited_rows: Set[int] = set()
    while True:
        if idx > prog_size:
            return False, "Overflow"
        if idx == prog_size:
            return True, acc
        if idx in visited_rows:
            return False, "Loop"
        visited_rows.add(idx)
        op, val = prog[idx]
        if op == "nop":
            idx += 1
        elif op == "acc":
            acc += val
            idx += 1
        elif op == "jmp":
            idx += val
        else:
            raise ValueError("bad op code")


def run():
    input = open("input/day8.txt", "r").read()
    data = p_file.parse(input)

    print("=== day 8 ===")

    res = execute_part1(data)
    print("Solution part 1:{}".format(res))

    for mut in mutations(data):
        res, ret = execute_part2(mut)
        if res:
            print("Solution part 2:{}".format(ret))
run()
