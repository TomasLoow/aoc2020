from typing import List

def chars_to_bindigits(line : str) -> List[int]:
    r = []
    for c in line:
        r += {"F":[0], "L":[0], "B":[1], "R":[1]}.get(c, [])
    return r

def bindigits_to_int(l : List[int]) -> int:
    acc = 0
    for b in l:
        acc = 2*acc + b
    return acc

def run():
    print("=== day 5 ===")
    input = open('input/day5.txt', 'r').readlines()
    parsed = [chars_to_bindigits(line) for line in input]
    place_ids = [bindigits_to_int(b) for b in parsed]
    print(f"Solution part 1: {max(place_ids)}")

    place_ids.sort()

    for pos, nextpos in zip(place_ids, place_ids[1:]):
        if nextpos - pos == 2:
            print(f"Solution part 2: {pos + 1}")

run()
