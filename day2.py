from parsy import generate, regex

from common_parsers import number, dash, space, colon, nl


@generate
def line_parser():
    min_count = yield number
    yield dash
    max_count = yield number
    yield space
    identifier = yield regex("[^:]*")
    yield colon
    yield space
    code = yield regex(".*")
    yield nl
    return (min_count, max_count, identifier, code)


def validate_line_problem_1(min_count, max_count, char, code):
    count = code.count(char)
    return min_count <= count <= max_count


def validate_line_problem_2(pos_1, pos_2, char, code):
    char_1 = code[pos_1-1]
    char_2 = code[pos_2-1]
    v_1 = 1 if char_1 == char else 0
    v_2 = 1 if char_2 == char else 0
    return (v_1 + v_2 == 1)


def run():
    lines = open('input/day2.txt', 'r').readlines()

    # Solve problem 1
    num_valid = 0
    for line in lines:
        p = line_parser.parse(line)
        if validate_line_problem_1(*p):
            num_valid +=1

    print("=== day 2 ===")
    print("Solution part 1:{}".format(num_valid))

    # Solve problem 2
    num_valid = 0
    for line in lines:
        p = line_parser.parse(line)
        if validate_line_problem_2(*p):
            num_valid +=1

    print("Solution part 2:{}".format(num_valid))

run()
