import re

from parsy import generate, string

# File parsers definitions
from common_parsers import word, space, number, nl


@generate
def p_bag():
    name = yield word
    while True:
        yield space
        w = yield word
        if w in ["bag", "bags"]:
            return name
        name += f" {w}"

@generate
def p_bag_count():
    count = yield number.desc("a bag count")
    yield space.desc("space between name and \"bag(s)\"")
    name = yield p_bag
    return (count, name)

@generate
def p_bag_simple():
    name = yield p_bag
    yield string(" contain no other bags.")
    return (name, [])

@generate
def p_bag_line_complex():
    name = yield p_bag
    yield space + string("contain") + space
    content = yield p_bag_count.sep_by(string(", "))
    yield string(".")
    return (name, content)

p_file = (p_bag_simple | p_bag_line_complex).sep_by(nl).map(dict)  # Parser for the whole file for part 1

# END parser defintions

def contains_gold(d, key):
    if key == "shiny gold":
        d[key] = False
        return False
    val = d[key]
    if type(val) == bool:  # value already memoized
        return val
    for (c, bag_name) in val:
        if bag_name == "shiny gold":
            d[key] = True
            return True
        else:
            if contains_gold(d, bag_name):
                d[key] = True
                return True
    d[key] = False
    return False


def count_bags_in(d, key):
    val = d[key]
    if type(val) == int:  # value already memoized
        return val
    count = 0
    for (c, bag_name) in val:
        count += c*(1 + count_bags_in(d, bag_name))
    d[key] = count
    return count

def run():
    input = open("input/day7.txt", "r").read()

    data = p_file.parse(input)
    print(data)

    print("=== day 7 ===")
    for key in list(data.keys()):
        contains_gold(data, key)
    count_gold = 0
    for k,v in data.items():
        if v:
            count_gold += 1

    print("Solution part 1:{}".format(count_gold))

    # part two
    data = p_file.parse(input)
    import sys
    print("Solution part 1:{}".format(count_bags_in(data, "shiny gold")))

run()
