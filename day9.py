from functools import lru_cache
from typing import List, Tuple, Generator, Union, Set

from parsy import regex, string, seq, alt

from common_parsers import nl, number
from common_functions import knapsack

# File parsers definitions

p_file = (number.sep_by(nl) << nl)

# END parser defintions


def run():
    input = open("input/day9.txt", "r").read()
    data = p_file.parse(input)

    print("=== day 9 ===")
    # reuse the knapsack function from day 1
    for i in range(26, len(data)):
        current = data[i]
        prev_25 = data[i-25:i]
        prev_25.sort()
        pair = knapsack(current,prev_25,2)
        if pair is None:
            ans_1 = current
            break
    print("Solution part 1:{}".format(ans_1))

    #Memoized function
    @lru_cache(maxsize=200000)
    def f(dist, i):
        if dist == 0:
            return data[i]
        part = f(dist - 1, i + 1)
        return data[i] + part

    for dist in range(2, len(data)):
        for i in range(0,len(data)-dist):
            r = f(dist, i)
            if r == ans_1:
                sequence = data[i:i+dist]
                ans_2 = min(sequence)+max(sequence)
                print("Solution part 2:{}".format(ans_2))
                return
run()
