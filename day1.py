from common_functions import knapsack

def run():
    l = [int(line) for line in open('input/day1.txt','r').readlines()]
    l.sort()


    print("=== day 1 ===")

    [a,b] = knapsack(2020,l,2)
    print("Solution part 1:{}".format(a*b))

    [a,b,c] = knapsack(2020,l,3)
    print("Solution part 2:{}".format(a*b*c))

run()
