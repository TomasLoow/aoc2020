def knapsack(target, list, num):
    if num == 1 :
        if target in list:
            return [target]
        else:
            return None

    if not list:
        return None
    h = list[0]
    r = list[1:]
    if h > target:
        return None
    res = knapsack(target-h, r, num-1)
    if res:
        return [h]+res
    res = knapsack(target,r,num)
    return res
