def trees_in(map, right=1, down=1):
    height = len(map)
    width = len(map[0])

    count = 0
    pos_x = 0
    pos_y = 0
    while pos_y < height:
        if map[pos_y][pos_x]:
            count +=1
        pos_x += right
        pos_x = pos_x % width
        pos_y += down
    return count


def parse_map(filename):
    lines = open(filename, 'r').readlines()
    map = []
    for l in lines:
        row = []
        for c in l:
            if c == "#":
                row.append(True)
            if c == ".":
                row.append(False)
        if row:
            map.append(row)
    return map


def run():
    map = parse_map('input/day3.txt')

    print("=== day 3 ===")
    trees = trees_in(map, right=3, down=1)
    print("Solution part 1:{}".format(trees))

    sloops= [
        {"right":1, "down":1},
        {"right":3, "down":1},
        {"right":5, "down":1},
        {"right":7, "down":1},
        {"right":1, "down":2}
    ]
    product = 1
    for sloop in sloops:
        trees = trees_in(map, **sloop)
        product *=trees

    print("Solution part 1:{}".format(product))



run()