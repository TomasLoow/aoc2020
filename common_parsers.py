from parsy import string, regex

colon = string(":")
dash = string("-")
nl = string("\n")
non_space = regex("[^ \n]+")
number = regex("[0-9]+").map(int)
space = string(" ")
word =regex("[a-z]+")
